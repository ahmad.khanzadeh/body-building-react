export const exerciseOptions={
    method: 'GET',
    headers: {
      'X-RapidAPI-Key': process.env.REACT_APP_API_KEY ,
      'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com'
    }
  };

export const fetchData= async (url, options) =>{
    const response= await fetch(url,options);
    const data=await response.json();

    return data;
}



export const youtubeOptions = {
  method: 'GET',
  headers: {
    // 'X-RapidAPI-Key': '7ff9bfd15dmshb8eaa47dfdb7382p19ee77jsnd1b46c15f4fa',
    'X-RapidAPI-Key': process.env.REACT_APP_API_KEY ,
    'X-RapidAPI-Host': 'youtube-search-and-download.p.rapidapi.com'
  }
};