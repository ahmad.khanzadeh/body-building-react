import React from 'react';
import {Box, Typography, Button} from '@mui/material';
import HeroBannerImage from '../assets/images/banner.jpg';

 const HeroBanner = () => {
  return (
    <Box sx={{mt:{lg:'212px',sx:'70px'}, ml:{sm:'50px' }}} position="relative" p="20px">
        <Typography color="#8B8000" fontWeight="600" fontSize="76px" variant='h1'>
            Fitness Club
        </Typography>
        <Typography fontWeight={700} sx={{fontSize:{lg:'44px', xs:'40px'},mb:"32px",mt:'30px'}  }>
            Sweat, Smile <br /> and Repeat
        </Typography>
        <Typography  fontSize='22px' lineHeight='35px' mb={3}>
            Check out the most effective exercises
        </Typography>
        <Button variant="contained" color="success" href="#exercises" mb='4' sx={{padding:'15px'}}> Explore Exercises</Button>
        <Typography fontSize='200px' fontWeight={600} color='#C99700' sx={{opacity:0.1,display:{lg:'block', xs:'none'}}}>
            Exercise
        </Typography>
        <img src={HeroBannerImage} alt="banner" className="hero-banner-img"/>
    </Box>
  )
}

export default HeroBanner
