import React, {useContext} from 'react'
import {Box, Typography} from '@mui/material';
import BodyPart from './BodyPart';
import {ScrollMenu,VisibilityContext} from 'react-horizontal-scrolling-menu';
import RightArrowIcon from '../assets/icons/right-arrow.png';
import LeftArrowIcon from '../assets/icons/left-arrow.png'
import ExerciseCard from './ExerciseCards'

const LeftArrow = () => {
  const { scrollPrev } = useContext(VisibilityContext);

  return (
    <Typography onClick={() => scrollPrev()} className="right-arrow">
      <img src={LeftArrowIcon} alt="right-arrow" />
    </Typography>
  );
};

const RightArrow = () => {
  const { scrollNext } = useContext(VisibilityContext);

  return (
    <Typography onClick={() => scrollNext()} className="left-arrow" sx={{ width:'2px'}}>
      <img src={RightArrowIcon} alt="right-arrow" />
    </Typography>
  );
};
const HorizontalScrollbar = ({data, bodyPart, setBodyPart, isBodyParts}) => {
  
  return (
    <ScrollMenu LeftArrow={LeftArrow} RightArrow={RightArrow}>
      {/* for the last time: .map(()=>()) and not {}*/}
      {data.map((item)=>(
                          <Box key={item.id || item} itemID={item.id || item} title={item.id || item} m="0 40px">
                            {/* bodypart from searchExersices.js ===>  they were from home.js. isBodyPart was added due to mixture error in related exercise and realted equipment (second element will be rendered) */}
                            {isBodyParts ? <BodyPart item={item}  bodyPart={bodyPart} setBodyPart={setBodyPart} /> : <ExerciseCard  exercise={item}/> }
                                
                          </Box>
                   )
      )}
    </ScrollMenu>
  )
}

export default HorizontalScrollbar