import React,{useEffect, useState }from 'react'
import Pagination from '@mui/material/Pagination'
import {Box,Stack,Typography} from '@mui/material'

import { fetchData, exerciseOptions } from '../utils/FetchData'
import ExerciseCard from './ExerciseCards'

// remember: arguments should be passed in  '{}' --- this makes a long time to debug!
const Exercises=({exercises, setExercises,bodyPart})=> {
  const [currentPage, setCurrentPage]=useState(1);
  const exercisesPerPage=9;
  const indexOfLastExercise= currentPage *exercisesPerPage;
  const indexOfFirstExercise= indexOfLastExercise - exercisesPerPage;
  const currentExercises= exercises.slice(indexOfFirstExercise, indexOfLastExercise);

  const paginate=(e, value)=>{
      setCurrentPage(value);
      window.scrollTo({top:1730, behavior:'smooth'})
  }
  // to display exercises based on user's clicks ( on different body parts)
  useEffect(()=>{
      const fetchExercisesData= async ()=>{
        // fetch exercises again. If nothing was selected=> reflect 'all'
        //                         if any option was selected => again fetch that ${bodyPart}
        let exercisesData=[];
        if(bodyPart ==='all'){
          exercisesData= await fetchData('https://exercisedb.p.rapidapi.com/exercises',exerciseOptions);
        }else{
          exercisesData= await fetchData(`https://exercisedb.p.rapidapi.com/exercises/bodyPart/${bodyPart}`,exerciseOptions);
        }

        setExercises(exercisesData);
      }

      fetchExercisesData();

    // recall entire function anytime that badyPart changes
  },[bodyPart])

  return (
    <Box id="exercises" 
    sx={{lg:{mt:'110px'}}}
    mt="50px"
    p="20px"
    >
      <Typography variant='h4' mb='46px' >
        Showing results...
      </Typography>
      <Stack direction='row' sx={{gap:{lg:'116px',xs:'52px'}}} flexWrap='wrap' justifyContent='center' >
        {currentExercises.map((exercise,idx)=>(<ExerciseCard key={idx} exercise={exercise}/>))}
      </Stack>
      <Stack mt='20px' alignItems='center' >
      {/* show pagination component if recived lenght if more than 9 elements */}
        {exercises.length >9 && (
          <Pagination 
            color='standard'
            shape='rounded'
            count={Math.ceil(exercises.lenght /exercisesPerPage)}
            page={currentPage}
            onChange={paginate}
            size='large'
          />
        )}
      </Stack>
    </Box>
  )
}

export default Exercises