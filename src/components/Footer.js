import React from 'react'
import {Box, Stack, Typography} from '@mui/material'
import FooterLogo from '../assets/images/Logo-1.jpg'

const Footer = () => {
  return (
    <Box mt="80px" bgcolor='#A7FF19' color='#000'>
      <Stack gap="40px" alignItems='center' px="40px" pt='24px'>
        <img src={FooterLogo} alt="developer logo" width='200px' height='40px' />
        <Typography variant='h6' pb='40px' mt='20px' >
          Created by  Khanzadeh- powered by Rapid API. <br />
          <small> Feel free to clone it from : <a href='https://gitlab.com/ahmad.khanzadeh' > Gitlab </a></small>
        </Typography>
      </Stack>
    </Box>
  )
}

export default Footer