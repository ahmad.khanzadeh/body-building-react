import React from 'react'
import {Box, Stack, Typography} from '@mui/material'
import HorizontalScrollbar from '../components/HorizontalScrollbar'
import Loader from './Loader'

const SimilarExercises = ({targetMuscleExercises,equipmentExercises}) => {
  return (
    <Box sx={{mt:{lg:'80px', xs:'5px'}}}>
      <Typography variant='h3'mb={5}>Exercises that target the same muscle group: </Typography>
      <Stack direction="row" sx={{p:'2', position:'relative'}}>
        {targetMuscleExercises.length ? <HorizontalScrollbar data={targetMuscleExercises} key={Math.random()}/>
                                      : <Loader />  
      } 
      </Stack>

      <Typography variant='h3'mb={5}>Exercises which use the same equipment: </Typography>
      <Stack direction="row" sx={{p:'2', position:'relative'}}>
        {equipmentExercises.length ? <HorizontalScrollbar data={equipmentExercises} key={Math.random()}/>
                                      : <Loader />  
      } 
      </Stack>
    </Box>
  )
}

export default SimilarExercises