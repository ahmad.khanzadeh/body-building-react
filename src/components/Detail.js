import React from 'react';
import { Typography, Stack, Button } from '@mui/material';

import BodyPartImage from '../assets/icons/body-part.png';
import TargetImage from '../assets/icons/target.png';
import Equipment from '../assets/icons/equipment.png';

const Detail = ({exerciseDetail}) => {
   const {bodyPart, gifUrl, name, target, equipment} = exerciseDetail;
   const extraDetail =[
    {
        icon: BodyPartImage,
        name: bodyPart
    },
    {
        icon: TargetImage,
        name: target
    },
    {
        icon: Equipment,
        name: equipment
    }
   ]
  return (
    <Stack gap="60"  sx={{flexDirection:{lg:'row', xs:'column'}, p:'20px',alignItems:'center'}}>
        <img  src={gifUrl} alt={name} loading="lazy" className='detail-image'/>
        <Stack sx={{gap:{lg:'35px', xs:'15px'}, margin:{lg:'20px'}}}>
            <Typography variant='h3'>
                {name}
            </Typography>
            <Typography variant='h6'>
                Exercises keeps you strong. {name} {` `} is one of the best exercises to target your  {target}.
                It will help you improve your mood and lose weight.
            </Typography>
            {/* show 3 images and description for each exercise ---  */}
            {extraDetail.map((item)=>(
                <Stack key={item.name} direction='row' gap="24px" alignItems='center' > 
                        <Button sx={{backgroundColor:'#5F9EA0',borderRadius:'50%', width:'80px', height:'80px'}}>
                            <img src={item.icon} width="50px" alt={bodyPart} style={{width:'50px', height:'50px',}}/>
                        </Button>
                        <Typography textTransform='capitalize' variant='h6'>
                            {item.name}
                        </Typography>
                </Stack>
            ))}
        </Stack>
    </Stack>
  )
}

export default Detail