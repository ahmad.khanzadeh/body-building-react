import React from 'react';
import {useState, useEffect} from 'react';
import {Box,Button, Typography,Stack,TextField} from '@mui/material'
import { fetchData,exerciseOptions } from '../utils/FetchData';
import HorizontalScrollbar from './HorizontalScrollbar';


const SearchExcersices=({setExercises,bodyPart,setBodyPart})=> {
  // to make a container in which we save the 'search' exercise  user had searched
  const [search,setSearch]=useState('');

  const [bodyParts,setBodyParts]=useState([]);

  // to get different categoris
  useEffect(()=>{
    const fetchExercisesData= async () =>{
      const bodyPartsData= await fetchData('https://exercisedb.p.rapidapi.com/exercises/bodyPartList', exerciseOptions);

      setBodyParts(['all',...bodyPartsData])
    }
    fetchExercisesData();
  }, [])
  const handelSearch= async ()=>{
        if(search){
          const exerciseData=await fetchData('https://exercisedb.p.rapidapi.com/exercises',exerciseOptions);
          const searchedExercises=exerciseData.filter(
            (exercise) =>exercise.name.toLowerCase().includes(search)||
                         exercise.target.toLowerCase().includes(search)||
                         exercise.equipment.toLowerCase().includes(search)||
                         exercise.bodyPart.toLowerCase().includes(search)
            
          );
          setSearch('');
          setExercises(searchedExercises);
        }
  }
  return (
    <Stack alignItems='center' justifyContent='center' mt='37px' p='20px'>
      <Typography fontWeight={700} sx={{fontSize:{lg:'44px', xs:'30px'}}} mb='50px' textAlign='center'>
        Awesome Exersices You <br/> Should know
      </Typography>
      <Box position='relative' mb='72px' >
        <TextField 
          height='76px'
          value= {search}
          onChange={(e) => {setSearch(e.target.value.toLowerCase())}}
          placeholder='Search Exercises'
          type='text'
          sx={{input:{fontWeight:'700', border: 'none' , borderRadius:'4px'}, width:{lg:'800px', xs:'350px'}, backgroundColor:'#fff'}}
        />
        <Button className='Search-btn' sx={{bgcolor:'#BF40BF',color:'#fff',textTransform:'none', width:{lg:'185px',xs:'80px'}, height:'56px',fontSize:{lg:'20px',xs:'14px'}, position:'absolute', right:'0' }} onClick={handelSearch}>
            Search
        </Button>
      </Box>
      {/* different body part exercises component */}
      <Box sx={{position:'relative',width:'100%',p:'20px'}}>
        <HorizontalScrollbar  data={bodyParts} bodyPart={bodyPart} setBodyPart={setBodyPart} isBodyParts/>
      </Box>
    </Stack>
  )
}

export default SearchExcersices