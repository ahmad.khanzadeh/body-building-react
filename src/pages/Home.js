import React ,{useState}from 'react';
import {Box} from '@mui/material';

import HeroBanner from '../components/HeroBanner';
import SearchExcersices from '../components/SearchExcersices';
import Exercises from '../components/Exercises';
const Home = () => {
    // to collect different exercises fetched from server
    const [exercises, setExercises] = useState([]);
    const [bodyPart,setBodyPart]=useState('all');
   
  return (
    <Box>
      <HeroBanner />
      <SearchExcersices  setExercises={setExercises} bodyPart={bodyPart} setBodyPart={setBodyPart}/>
      <Exercises setExercises={setExercises} exercises={exercises} bodyPart={bodyPart} />
    </Box>
  )
}

export default Home