import React, {useEffect, useState} from 'react';
// to get the id of current exercises
import {useParams} from 'react-router-dom';
import {Box} from '@mui/material';
import {exerciseOptions, fetchData, youtubeOptions} from '../utils/FetchData'

import Detail from '../components/Detail';
import ExerciseVideos from '../components/ExerciseVideos';
import SimilarExercises from '../components/SimilarExercises';


const ExerciseDetail = () => {
  const [exerciseDetail, setExerciseDetail] = useState({});
  const {id} = useParams();
  const [exerciseVideos, setExerciseVideos] =useState([]);
  // similar equipment 
  const [equipmentExercises, setEquipmentExercises]=useState([]);
  // similar exercises
  const [targetMuscleExercises, setTargetMuscleExercises]=useState([]);
  useEffect(()=>{
    const fetchExercisesData = async () => {
      const exerciseDbUrl = 'https://exercisedb.p.rapidapi.com';
      const youtubeSearchUrl = 'https://youtube-search-and-download.p.rapidapi.com';
      // call the exercise
      const exerciseDetailData = await fetchData(`${exerciseDbUrl}/exercises/exercise/${id}`, exerciseOptions);
      // set fetched data to hoocks
      setExerciseDetail(exerciseDetailData)

      // second api--getting video from youtube
      const exerciseVideosData= await fetchData(`${youtubeSearchUrl}/search?query=${exerciseDetailData.name}`,youtubeOptions);
      setExerciseVideos(exerciseVideosData.contents);
      

      // to fetch similar exercises from server---displayed in exercise detail
      const targetMuscleExercisesData = await fetchData(`${exerciseDbUrl}/exercises/target/${exerciseDetailData.target}`,exerciseOptions);
      setTargetMuscleExercises(targetMuscleExercisesData);
      // to fetch similar equipment from server---displayed in exercise detail
      const equipmentExercisesData = await fetchData(`${exerciseDbUrl}/exercises/equipment/${exerciseDetailData.equipment}`,exerciseOptions);
      setEquipmentExercises(equipmentExercisesData);
    };
    fetchExercisesData();
  }, [id]);

  return (
    <Box>
      <Detail exerciseDetail={exerciseDetail}/>
      <ExerciseVideos exerciseVideos={exerciseVideos} name={exerciseDetail.name}/>
      <SimilarExercises targetMuscleExercises={targetMuscleExercises} equipmentExercises={equipmentExercises}/>
    </Box>
  )
}

export default ExerciseDetail